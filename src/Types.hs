import qualified Data.Map.Strict as M

-- |A stub for resource types.
type Resource = Int

-- |Population size, amount of resources and so on.
-- |Beware of atto-foxes! 
type Quantity = Double

-- |Many resources.
type ResMap = M.Map Resource Quantity

-- |Trait of species.
data Trait = Trait
    { trName   :: !String -- ^ Arbitrary name
    , trEffect :: !Effect -- ^ Effect
    }

-- |A stub for trait effects.
data Effect = Effect

-- |Species.
data Species = Species
    { spName   :: !String  -- ^ Arbitrary name
    , spTraits :: [Trait]  -- ^ List of traits
    , spBreed  :: !Double  -- ^ Breeding performance
    }

-- |A stub for players.
type Player = String

-- |A population of some species.
data Population = Population
    { ppSpecies :: !Species  -- ^ Species
    , ppOwner   :: !Player   -- ^ Owner of the population
    , ppSize    :: !Quantity -- ^ Size of the population
    }

data AreaType = Land | Water

-- |An area of the game map.
data Area = Area
    { atType        :: !AreaType
    , arLuminocity  :: !Quantity    -- ^ Amount of incoming light
    , arTemperature :: !Double      -- ^ Temperature
    , arResources   :: !ResMap      -- ^ Accumulated resources
    , arPopulations :: [Population] -- ^ Inhabiting populations
    }

